package everything.started.with.a.single.click.authentication;

import everything.started.with.a.single.click.repository.CustomerRepository;
import everything.started.with.a.single.click.repository.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 * Created by shiro on 12/22/16.
 */

@Component
public class CustomAuthentication  implements AuthenticationProvider{
    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Customer um=  customerRepository.findCustomerByUsernameAndPassword(
                (String)authentication.getPrincipal(),
                (String)authentication.getCredentials());
        if(um == null) throw new BadCredentialsException("Invalid login data");

        return new UsernamePasswordAuthenticationToken(um.getUsername(),null,um.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(aClass);
    }
}

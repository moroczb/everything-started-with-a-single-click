package everything.started.with.a.single.click.controller;

import everything.started.with.a.single.click.repository.model.Rank;
import everything.started.with.a.single.click.repository.RankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shiro on 12/23/16.
 */
@RestController
@RequestMapping(value="/rank")
public class RankController {
    @Autowired
    RankRepository rankRepository;

    @RequestMapping(value="/new", method = RequestMethod.POST)
    public String newRank(@RequestParam(name="title") String title){
        try{
            rankRepository.save(new Rank(title));
            return "success";
        }catch(Exception e){
            return "failure";
        }
    }

    @RequestMapping(value="/show", method=RequestMethod.GET)
    public @ResponseBody
    List<Rank> show(){
        List<Rank> rls = new ArrayList<>();
         for(Rank r : rankRepository.findAll()){
             rls.add(r);
         }
         return rls;
    }
    @RequestMapping(value="/show", method=RequestMethod.POST)
    public @ResponseBody Rank show(@RequestParam(value="title") String title){
        return rankRepository.findRoleByTitle(title);
    }

}

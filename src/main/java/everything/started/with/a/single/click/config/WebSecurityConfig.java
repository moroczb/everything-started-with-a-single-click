package everything.started.with.a.single.click.config;

import everything.started.with.a.single.click.authentication.AuthFailureHandler;
import everything.started.with.a.single.click.authentication.AuthSuccessHandler;
import everything.started.with.a.single.click.authentication.CustomAuthentication;
import everything.started.with.a.single.click.authentication.ErrorAuthEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by shiro on 12/21/16.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    AuthSuccessHandler authSuccessHandler;
    @Autowired
    AuthFailureHandler authFailureHandler;
    @Autowired
    ErrorAuthEntryPoint errorAuthEntryPoint;

    @Autowired
    CustomAuthentication customAuthentication;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests().antMatchers("/rank/show","/home/customer/show").permitAll().
                and().exceptionHandling().authenticationEntryPoint(errorAuthEntryPoint).
                and().formLogin().loginPage("/login").successHandler(authSuccessHandler).failureHandler(authFailureHandler).permitAll().
                and().logout().permitAll().
        and().authorizeRequests().antMatchers("/customer/new","/rank/new").authenticated();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(customAuthentication);

    }
}

package everything.started.with.a.single.click.repository.model;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by shiro on 12/23/16.
 */
@Entity
public class Rank implements GrantedAuthority{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique=true)
    private String title;


    public Rank() {
    }

    public Rank(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Rank{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    @Override
    public String getAuthority() {
        return title;
    }
}

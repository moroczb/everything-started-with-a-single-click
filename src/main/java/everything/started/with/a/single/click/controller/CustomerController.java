package everything.started.with.a.single.click.controller;

import everything.started.with.a.single.click.repository.model.Customer;
import everything.started.with.a.single.click.repository.model.Rank;
import everything.started.with.a.single.click.repository.CustomerRepository;
import everything.started.with.a.single.click.repository.RankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mórocz Bálint on 2016. 12. 20..
 */

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    RankRepository rankRepository;

    @RequestMapping(value="/new", method = RequestMethod.POST)
    public @ResponseBody String newUser(
            @RequestParam(value="username") String username,
            @RequestParam(value="password") String password,
            @RequestParam(value="role", required=false, defaultValue="USER") String roles
        ){
        try{
            List<Rank> rls = new ArrayList<>();
            for(String s : roles.split(",")){
                Rank r = rankRepository.findRoleByTitle(s);
                if(r == null) throw new InvalidParameterException("Invalid Rank title has been sent");
                rls.add(r);
            }
            customerRepository.save(new Customer(username,password, rls));
            return "success";
        }catch (Exception e){
            System.out.println(e.getMessage());
            return "failure";
        }
    }

    @RequestMapping(value="/show", method=RequestMethod.GET)
    public @ResponseBody
    List<Customer> show(){
        List<Customer> cls = new ArrayList<>();
        for(Customer c : customerRepository.findAll()){
            cls.add(c);
        }
        return cls;
    }

    @RequestMapping(value="/show", method=RequestMethod.POST)
    public @ResponseBody
    Customer show(@RequestParam(value="username") String username){
        return customerRepository.findCustomerByUsername(username);
    }
}

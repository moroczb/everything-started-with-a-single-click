package everything.started.with.a.single.click.testdata;

import everything.started.with.a.single.click.repository.CustomerRepository;
import everything.started.with.a.single.click.repository.RankRepository;
import everything.started.with.a.single.click.repository.model.Customer;
import everything.started.with.a.single.click.repository.model.Rank;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class TaskTestData implements ApplicationListener<ApplicationReadyEvent> {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RankRepository rankRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        Rank rank = new Rank("USER");
        rankRepository.save(rank);
        Customer customer = new Customer("user", "password", Collections.singletonList(rank));
        customerRepository.save(customer);
        logger.info("user added");
    }
}
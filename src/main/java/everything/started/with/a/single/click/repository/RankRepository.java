package everything.started.with.a.single.click.repository;

import everything.started.with.a.single.click.repository.model.Rank;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by shiro on 12/23/16.
 */
public interface RankRepository extends CrudRepository<Rank,Long> {
    Rank findRoleByTitle(String title);
}

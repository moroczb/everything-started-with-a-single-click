package everything.started.with.a.single.click.repository;

import everything.started.with.a.single.click.repository.model.Customer;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by shiro on 12/23/16.
 */
public interface CustomerRepository extends CrudRepository<Customer,Long> {

    Customer findCustomerByUsername(String username);
    Customer findCustomerByUsernameAndPassword(String username,String password);
}

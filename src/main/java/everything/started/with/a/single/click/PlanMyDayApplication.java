package everything.started.with.a.single.click;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlanMyDayApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlanMyDayApplication.class, args);
    }
}
